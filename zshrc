source ~/.zsh/aliases.zsh
source ~/.zsh/bindkey.zsh
source ~/.zsh/completion.zsh
source ~/.zsh/exports.zsh
source ~/.zsh/functions.zsh
source ~/.zsh/history.zsh
source ~/.zsh/iterm2.zsh
source ~/.zsh/path.zsh
source ~/.zsh/plugins.zsh
source ~/.zsh/prompt.zsh
source ~/.zsh/setopt.zsh
source ~/.zsh/theming.zsh

# Add my keys to the ssh agent; passwords are pulled from the keychain.
# The --apple-load-keychain option is unique to MacOS.
ssh-add --apple-load-keychain
