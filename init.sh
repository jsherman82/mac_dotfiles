#!/bin/bash

# get rid of the current files
rm -rfv $HOME/.config/alacritty
rm -rfv $HOME/.ssh/config
rm -rfv $HOME/.zsh
rm -rfv $HOME/.zshrc

# link files
ln -s $HOME/.dotfiles/alacritty $HOME/.config/alacritty
ln -s $HOME/.dotfiles/ssh_client_config $HOME/.ssh/config
ln -s $HOME/.dotfiles/zshrc $HOME/.zshrc
ln -s $HOME/.dotfiles/zsh $HOME/.zsh
